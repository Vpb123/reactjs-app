import React, { Component } from 'react';
import {Card, CardImg,CardText,CardBody,CardTitle,Breadcrumb,BreadcrumbItem,Row,Col,Modal,ModalBody,ModalHeader,Label,Button} from 'reactstrap';
import {Link} from 'react-router-dom';
import {Control,LocalForm,Errors} from 'react-redux-form';

const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => val && (val.length >= len);

class CommentForm extends Component{
   constructor(props){
     super(props);
     this.state={
       isModalOpen:false
     };
        
     this.handleSubmit=this.handleSubmit.bind(this)
     this.toggleModal= this.toggleModal.bind(this);
    }
   
    handleSubmit(values){
        console.log("Current State is:" + JSON.stringify(values));
        alert("Current State is:" +JSON.stringify(values));
    }

    toggleModal(){
      this.setState({
          isModalOpen: !this.state.isModalOpen
      });
  }


  render(){
    return(

        <React.Fragment>
          <Button className="bg-white text-dark" onClick={this.toggleModal}><i className="fa fa-pencil fa-lg"></i>{' '}Submit Comment</Button>
      <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
      <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
      <ModalBody>
           <LocalForm onSubmit={(values) =>this.handleSubmit(values)}>
            <Row className='form-group'>
            <Label htmlFor="author" md={5}><span>Your Name</span></Label>
                                <Col md={12}>
                                    <Control.text model=".author" id="author" name="author"
                                        placeholder="Author Name"
                                        className="form-control"
                                        validators={{
                                            minLength: minLength(3), maxLength: maxLength(15)
                                        }}
                                         />
                                    <Errors
                                        className="text-danger"
                                        model=".author"
                                        show="touched"
                                        messages={{
                                            required: 'Required',
                                            minLength: 'Must be greater than 2 characters',
                                            maxLength: 'Must be 15 characters or less'
                                        }}
                                     />
                                </Col>
            </Row>
            <Row className='form-group'>
            <Label htmlFor="rating" md={2}><span>Rating</span></Label>
            <Col md={12}>
            <Control.select model=".rating"className="form-control" id="rating" name="rating">
                  <option  value='1'>1</option>
                  <option value='2'>2</option>
                  <option  value='3'>3</option>
                  <option  value='4'>4</option>
                  <option  value='5'>5</option>
              </Control.select>
              </Col>
             
            </Row>
            <Row className="form-group">
                                <Label htmlFor="comments" md={2}><span>Comment</span></Label>
                                <Col md={12}>
                                    <Control.textarea model=".comments" id="comments" name="comments"
                                        rows="6"
                                        className="form-control" />
                                </Col>
            </Row>
            <Row className="form-group">
                                <Col md={{size:10, offset: 2}}>
                                    <Button type="submit" color="primary">
                                       Submit
                                    </Button>
                            </Col>
            </Row>
           </LocalForm>
      </ModalBody>
  </Modal>
  </React.Fragment>

 
    );
  }
}
  function RenderDish({dish})
  {
    if(dish!=null){
      return(
        <div className='col-md-5 m-1'>
        <Card>
        <CardImg width='100%' src={dish.image} alt={dish.name} />
        <CardBody>
              <CardTitle heading>{dish.name}</CardTitle>
              <CardText>{dish.description}</CardText>
        </CardBody>
        </Card>
        </div>
      )
    }
    else {
      return(
        <div></div>
      );
    }
  }
 function  RenderComments({comments})
  {
    if(comments!=null){
      const c=comments.map(comment =>
      {
      return(
       <li key={comment.id}>
       <p>{comment.comment}</p>
       <p>--{comment.author},
       &nbsp;
       {new Intl.DateTimeFormat('en-US', {
                           year: 'numeric',
                           month: 'long',
                           day: '2-digit'
          }).format(new Date(comment.date))}
       </p>
       </li>
      );
    });
    return(
      <div className="col-md-5 m-1">
      <h4>Comments</h4>
      <ul className="list-unstyled">
       {c}
       </ul>
       <CommentForm/>
      </div>
    );
    }
    else {
      return(
        <div></div>
      );
    }
  }
  
const DishDetail=(props)  =>{
    if(props.dish==null){
      return(<div></div>);
    }
    else{
    return(
      <div className='container'>
          <Breadcrumb>
          <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
          <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
          </Breadcrumb>
      <div className='row'>
       <RenderDish dish={props.dish}/>
       <RenderComments comments ={props.comments}/>
      </div>
      </div>
    );
  }

}
export default DishDetail;
